const express = require("express");
const app = express();
const mongoose = require("mongoose");
const bcrypt = require("bcrypt");

// creating the  Schema for registration of the user
const registerSchema = new mongoose.Schema({
  Name: {
    type: String,
    required: true,
    trim: true
    
  },
  Email: {
    type: String,
    required: true,
    unique: true,
  },
  number: {
    type: Number,
    required: true,
    unique: true,
    min:10,
    max:10000000000
  },
  Password: {
    type: String,
    required: true,
  },
});

// the pre save middleware to hash the password
registerSchema.pre("save", async function (next) {
  const salt = await bcrypt.genSalt(10);
  const hashedPassword = await bcrypt.hash(this.Password, salt);
  this.Password = hashedPassword;
  next();
});

// the method to cheak if the login password matchs to the user password which exist in the datavase
registerSchema.methods.correctPassword = async function (
  candidatePassword,
  userPassword
) {
  return await bcrypt.compare(candidatePassword, userPassword);
};

const register = mongoose.model("register", registerSchema);
module.exports = register;
