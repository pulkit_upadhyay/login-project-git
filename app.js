
// reqireing files and modules
const controler = require("./controlers")
const express = require("express");
const app = express();
const hostname = "0.0.0.0";
const port = process.env.PORT || 3000;
const hbs = require("hbs");
var bodyParser = require("body-parser");
app.use(bodyParser.json()); // to support JSON bodies
app.use(bodyParser.urlencoded({ extended: true }));
const { response } = require("express");
const cookieParser = require("cookie-parser");



// express middleware
app.use(express.static("public"));
//  setting view engene as hbs 
app.set("view engine", "hbs");
app.set("views", "views");


// the middleware for the cookies to be human readable
app.use(cookieParser());

// all get routes
app.get("/login", controler.logincontroll);
app.get("/forget_password", controler.forgetpasswordcontroll);
app.get("/register", controler.registercontroll);
app.get("/home", protect,  controler.homecontroll);
app.get("/reset-password/:_id/:resetToken", controler.resetpasswordcontroll);


// all post routes
app.post("/register",controler.postregister);
app.post("/login", controler.postlogin);
app.post("/logout", controler.postlogout)
app.post("/forget_password", controler.postforgetpassword);
app.post("/reset-password/:_id/:resetToken", controler.postresetpassword);




// the function for protacting the routes
async function protect(req, res, next) {
  let token = req.cookies.Token;
  if (!token) {
    res.sendStatus(403);
  } else {
    next();
  }
}







// sppinning up the server..............on port 3000.......
app.listen(port, hostname, () => {
  console.log(`app is listening on port ${port}`);
});
