const express = require("express");
const app = express();
const jwt = require("jsonwebtoken");
var bodyParser = require("body-parser");
app.use(bodyParser.json()); // to support JSON bodies
app.use(bodyParser.urlencoded({ extended: true }));
const mongoose = require("mongoose");
const { response } = require("express");
let name;
const JWT_SECRET = "mynameispulkitupadhyayfromharda";
const register = require("./scheema");
// nodemailer
const nodemailer = require("nodemailer");

// nedemailer transporter creation

let transporter = nodemailer.createTransport({
  service: "gmail.com",
  // host: 'login-project-git.herokuapp.com',
  // host: 'smtp.office365.com',
  port: 587,
  secure: false,
  auth: {
    user: "projectlogin57@gmail.com",
    pass: "pulkitlogin",
  },
});

// managing the scheema here
// connecting node js application to mongodb database
mongoose
  .connect(
    "mongodb+srv://pulkit:shraddhap@cluster0.hochl.mongodb.net/survey?retryWrites=true&w=majority",
    {
      useNewUrlParser: true,
      useCreateIndex: true,
      useFindAndModify: false,
    }
  )
  .then((con) => {
    //  console.log(con.connections)
    console.log("Datatabase connection successful");
  });

// all the route hendelers

exports.homecontroll = (req, res, next) => {
  jwt.verify(req.cookies.Token, JWT_SECRET, (err, authData) => {
    if (err) {
      res.sendStatus(403);
    } else {
      console.log(req.cookies);
      res.render("home.hbs", { name: name });
    }
  });
};

exports.logincontroll = (req, res, next) => {
  res.render("login");
};
exports.forgetpasswordcontroll = (req, res, next) => {
  res.render("forget");
};
exports.registercontroll = (req, res, next) => {
  res.render("register");
};
exports.resetpasswordcontroll = async (req, res, next) => {
  const { _id, resetToken } = req.params;

  console.log(_id, resetToken);
  const User = await register.findOne({ _id: _id });
  console.log(User);
  if (!User) {
    res.send("please enter valid email");
    next();
  }
  const secret = (await JWT_SECRET) + User.Password;
  try {
    const payload = jwt.verify(resetToken, secret);
    res.render("reset-password", { email: User.Email });
  } catch (error) {
    res.send(error.massage);
  }
};

//   all post routes

exports.postregister = (req, res, next) => {
  let userName = req.body.fullName;
  let useremail = req.body.Email;
  let userMobile = req.body.mobileNumber;
  let userPassword = req.body.password;

  const user = new register({
    Name: userName,
    number: userMobile,
    Email: useremail,
    Password: userPassword,
  });

  user.save().then((doc) => {
    console.log(doc);

    const token = jwt.sign({ id: user.__id }, JWT_SECRET, {
      expiresIn: "10d",
    });
    name = user.Name;

    res.cookie("Token", token, { httpOnly: true, maxAge: 1.728e8 });
    res.redirect("/home");
  });
};

exports.postlogin = async (req, res, next) => {
  const { Email, password } = req.body;

  if (!Email || !password) {
    return next("please enter valid email or password sp fdf");
  }

  // cheaking if the email exist in database

  const User = await register.findOne({ Email });
  console.log(User);

  if (!User || !(await User.correctPassword(password, User.Password))) {
    return next("enter the correcr cridentals");
  }
  console.log(User);

  const token = await jwt.sign({ id: User.__id }, JWT_SECRET, {
    expiresIn: "10d",
  });
  name = User.Name;
  res.cookie("Token", token, { httpOnly: true, maxAge: 1.728e8 });
  res.redirect("/home");
};

exports.postlogout = (req, res, next) => {
  jwt.verify(req.cookies.Token, JWT_SECRET, (err, authData) => {
    if (err) {
      res.sendStatus(403);
    } else {
      res.clearCookie("Token");
      res.redirect("/login");
    }
  });
};

exports.postforgetpassword = async (req, res, next) => {
  let email = req.body.email;
  let User = await register.findOne({ Email: email });
  console.log(User);
  if (!User) {
    res.json({
      massage: "plese provide a valid or registerd email",
    });
    return;
  }
  const secret = (await JWT_SECRET) + User.Password;
  const paylod = await {
    email: User.Email,
    id: User._id,
  };
  const restetToken = await jwt.sign(paylod, secret, { expiresIn: "15m" });
  const link =
    await `https://login-project-git.herokuapp.com/reset-password/${User._id}/${restetToken}`;
  console.log(link);
  await transporter.sendMail(
    {
      from: "no-replay@insta.com",
      to: User.Email,
      subject: "reset password",
      text: `Hello ${User.Name} your password reset link is generated please click on the link below ${link}`,
    },
    async  (err, data)=> {
      if (err) {
       await console.log("error occurd", err);
      } else {
       await console.log("email sent");
      }
    }
  );
};
exports.postresetpassword = async (req, res, next) => {
  const { _id, resetToken } = req.params;
  const { password, confirmPassword } = req.body;
  const User = await register.findOne({ _id: _id });

  if (!User) {
    res.send("please enter valid email");
    next();
  }

  const secret = (await JWT_SECRET) + User.Password;
  if (password !== confirmPassword) {
    res.send("please provide same passwords");
    return next();
  }

  const payload = await jwt.verify(resetToken, secret);
  User.Password = await password;

  User.save().then((doc) => {
    console.log(doc);

    const token = jwt.sign({ id: User.__id }, JWT_SECRET, {
      expiresIn: "10d",
    });
    name = User.Name;

    res.cookie("Token", token, { httpOnly: true, maxAge: 1.728e8 });
    res.redirect("/home");
  });
};
